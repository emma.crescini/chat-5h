import { createApp } from 'vue'
import App from './App.vue'
import Home from './Home.vue'
import About from "./About.vue"
import WatchVideo from "./WatchVideo.vue"
import {createRouter, createWebHistory} from "vue-router";

const routes = [
    { path: '/', component: Home },
    { path: '/about', component: About },
    { path: '/watch?id=:id', name: 'watch',component: WatchVideo },
    //{ path: '/video-list', component: null },
  ]

//creo l'istanza router e gli passo le rotte
const router= createRouter({
    history: createWebHistory(),
    routes: routes,
})

//creo l'applicazione view
const app=createApp(App);
//gli dico di usare il router
app.use(router);
//lo monto sul tag
app.mount('#app');
